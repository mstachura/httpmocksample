﻿namespace HttpMockingTests
{
    using HttpMock;
    using HttpMocking.Suppliers;
    using NUnit.Framework;

    [TestFixture]
    public class MySupplierComponentTests
    {
        [Test]
        public void AvailablityAsPostShouldReturnContent()
        {
            const string expected = "Post response from Supplier HttpMock";

            var stubHttp = HttpMockRepository.At("http://localhost:9191");

            stubHttp.Stub(x => x.Post("/availability"))
                .Return(expected)
                .OK();

            var supplierImpl = new MySupplierComponent();
            var result = supplierImpl.AvailablityAsPost("http://localhost:9191/availability",
                "{\"email\":\"user@test.com\",\"password\":\"welc0me\"}");
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void AvailablityAsGetShouldReturnContent()
        {
            const string expected = "Hello from MySupplier HttpMock";

            var stubHttp = HttpMockRepository.At("http://localhost:9191");

            stubHttp.Stub(x => x.Get("/availability"))
                .Return(expected)
                .OK();

            var supplierImpl = new MySupplierComponent();
            var result = supplierImpl.AvailablityAsGet("http://localhost:9191/availability");
            Assert.That(result, Is.EqualTo(expected));
        }
    }
}
