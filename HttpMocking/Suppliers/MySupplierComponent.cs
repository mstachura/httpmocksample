﻿namespace HttpMocking.Suppliers
{
    using System.Net;

    public class MySupplierComponent
    {
        public string AvailablityAsGet(string url)
        {
            using (var webClient = new WebClient())
            {
                return webClient.DownloadString(url);
            }
        }

        public string AvailablityAsPost(string url, string parameters)
        {
            using (var webClient = new WebClient())
            {
                webClient.Headers[HttpRequestHeader.ContentType] = "application/vnd.api+json";
                return webClient.UploadString(url, parameters);
            }
        }
    }
}
