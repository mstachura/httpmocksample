﻿namespace HttpMocking
{
    using System;
    using Suppliers;

    internal class Program
    {
        private static void Main(string[] args)
        {
            var supplier = new MySupplierComponent();
            var availability = supplier.AvailablityAsGet("https://www.google.pl");
            Console.WriteLine(availability);
            Console.WriteLine("=======================");
            Console.ReadLine();

            availability = supplier.AvailablityAsPost("http://localhost:3000/auth",
                "{\"email\":\"user@test.com\",\"password\":\"welc0me\"}");
            Console.WriteLine(availability);
            Console.ReadLine();
        }
    }
}
